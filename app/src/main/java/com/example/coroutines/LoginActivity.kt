package com.example.coroutines

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.get
import com.example.coroutines.databinding.ActivityLoginBinding

class LoginActivity : AppCompatActivity() {

    private lateinit var loginViewModel: LoginViewModel
    private lateinit var binding: ActivityLoginBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityLoginBinding.inflate(layoutInflater)
        setContentView(binding.root)

        loginViewModel = ViewModelProvider(this).get()

        loginViewModel.loginValid.observe(this) {
            if (binding.loading.isVisible()) binding.loading.show(false)
            toast(if (it) "Success" else "Failure")
        }

        binding.submit.setOnClickListener {
            binding.loading.show(true)
            loginViewModel.onSubmitClicked(
                binding.username.text.toString(),
                binding.password.text.toString()
            )
        }
    }


}


