package com.example.coroutines

import android.content.Context
import android.view.View
import android.widget.ProgressBar
import android.widget.Toast

fun Context.toast(message: String, duration: Int = 0) {
    Toast.makeText(this, message, duration).show()
}

fun ProgressBar.show(visible: Boolean) {
    if (visible) this.visibility = View.VISIBLE else this.visibility = View.GONE
}

fun ProgressBar.isVisible(): Boolean{
   return if(this.visibility== View.VISIBLE) true else false
}